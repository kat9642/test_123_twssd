# Fetching of email and calculation of email sentiment
A python implementation of fetchmaildata using Flask as an HTTP interface


## Quick start

You should have access to an OpenShift cluster and be logged in with the
`oc` command line tool.


1. Launch fetchmaildata
   ```bash
oc new-app python:3.4~http://10.200.202.230/ashish/cal-daily-pdf-sentiment-call.git --name cal-email-sentiment-demo -e FILELOCATION=/data/backup/Redhat_files/data/email/prop/email.json
   ```

1. Expose an external route
   ```bash
   oc expose svc/fetchmaildata
   ```

1. Visit the exposed URL with your browser or other HTTP tool, for example:
   ```bash
   $ curl http://`oc get routes/fetchmaildata --template='{{.spec.host}}'`
   Python Flask fetchmaildata server running. Add the 'fetchmaildata' route to this URL to invoke the app.

   $ curl http://`oc get routes/fetchmaildata --template='{{.spec.host}}'`/fetchmaildata
  Check Data In Table
   ```

### Optional parameter
Commited changes 
If you would like to change the number of samples that are used to calculate
Pi, you can specify them by adding the `scale` argument to your request
, for example:
### Commiting changes for v2
```bash
$ curl http://`oc get routes/fetchmaildata --template='{{.spec.host}}'`/fetchmaildata
  Check Data In Table
```