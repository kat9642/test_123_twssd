from flask import Flask
from flask import request
import os 
import glob, os
import nltk
from flask import Flask
from flask import request
from os import environ
import json
import io
import pandas as pd
import requests
import tensorflow as tf
import numpy as np
import keras as k
import sklearn
import scipy

app = Flask(__name__)
# global identifier


@app.route("/")
def index():
    return "Python Microservice To fetch pdf survey data"

@app.route("/fetchmaildata")
def fetchmaildata():
    print("Tensorflow")
    print(tf.__version__) 
    print("Pandas")
    print(pd.__version__)
    print("Keras")
    print(k.__version__)
    print("NLTK")
    print(nltk.__version__)
    print("Scipy")
    print(scipy.__version__)
    print("Scikit")
    print(sklearn.__version__)
    return "Successfully Deployed All Dependencies. Check Logs to Find the Versions"
        
if __name__ == "__main__":
    port = int(os.environ.get("PORT", 8080))
    app.run(host='0.0.0.0', port=port)